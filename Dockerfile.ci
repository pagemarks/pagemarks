#
# pagemarks - Free, git-backed, self-hosted bookmarks
# Copyright (c) 2019-2021 the pagemarks contributors
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License, version 3, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/gpl.html>.
#

# Defines a container image used in our CI pipelines, hopefully speeding them up.

FROM python:3.10-slim-bullseye

ENV  DEBIAN_FRONTEND=noninteractive

RUN  python -m pip install --upgrade pip && \
     pip install virtualenv && \
     apt-get update && \
     for i in $(seq 1 8); do mkdir -p "/usr/share/man/man${i}"; done && \
     apt-get install --yes --no-install-recommends --no-install-suggests git && \
     apt-get install --yes ca-certificates nodejs npm && \
     apt-get autoremove --yes && \
     rm -rf /var/lib/apt/lists/* && \
     echo "Python: $(python -V)" && \
     echo "PIP: $(pip -V)" && \
     echo "NodeJS: $(node -v)" && \
     echo "NPM: $(npm -v)"
