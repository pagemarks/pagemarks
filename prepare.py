#
# pagemarks - Free, git-backed, self-hosted bookmarks
# Copyright (c) 2019-2021 the pagemarks contributors
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License, version 3, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/gpl.html>.
#

# This script is invoked prior to running setuptools during the build.
# It generates some files which are packaged with the Pagemarks Python package.

import glob
import json
import os
import re
import shutil
import subprocess
import sys



def ensure_root() -> int:
    print('Checking that we are being called from the project root ... ', end='', flush=True)
    if os.getcwd() == os.path.dirname(os.path.realpath(__file__)):
        print('OK', flush=True)
        return 0
    print('no - ERROR')
    return 1



def run_npm_ci() -> int:
    print()
    try:
        executable = 'npm.cmd' if os.name == 'nt' else 'npm'
        cp = subprocess.run([executable, 'ci'])
        print()
        return cp.returncode
    except FileNotFoundError:
        print('ERROR: npm not found. The Pagemarks build requires npm 7 or higher.', file=sys.stderr)
        return 1



def prepare_sass_build():
    os.makedirs('build/sass', exist_ok=True)
    for f in glob.glob(os.path.join('pagemarks/resources/sass', '_*.scss')):
        shutil.copy(f, 'build/sass/')



def create_sass_main(theme_name: str) -> str:
    outfile_name = f"build/sass/main_{theme_name}.scss"
    with open("pagemarks/resources/sass/main.scss", "r") as fin:
        with open(outfile_name, "w") as fout:
            for line in fin:
                fout.write(line.replace('@@bootswatch_theme@@', theme_name))
    return outfile_name



def load_theme_list() -> dict:
    with open('pagemarks/resources/html/css-themes.json', 'r') as f:
        s = f.read()
        return json.loads(re.sub("^\s*//.*", "", s, flags=re.MULTILINE))



def compile_sass() -> int:
    prepare_sass_build()
    print('Compiling SASS to CSS in pagemarks/resources/css ... ', end='', flush=True)
    css_themes = load_theme_list()
    executable = 'sass.cmd' if os.name == 'nt' else 'sass'
    for theme_name in css_themes.keys():
        print(f"{theme_name} ... ", end='', flush=True)
        main_sass_file = create_sass_main(theme_name)
        cp = subprocess.run([os.path.join('node_modules', '.bin', executable),
                             '--load-path=node_modules',
                             '--style=compressed',  # set to 'expanded' for debugging CSS, else to 'compressed'
                             '--no-source-map',
                             '--color',
                             main_sass_file,
                             f"pagemarks/resources/css/pagemarks_{theme_name}.css"])
        if cp.returncode != 0:
            print('ERROR')
            return cp.returncode
    print('OK')
    return 0



def copy_tagify() -> None:
    print('Providing third-party scripts to Python packaging ... ', end='', flush=True)
    shutil.copy('node_modules/@yaireo/tagify/dist/tagify.min.js', 'pagemarks/js/vendor/')
    shutil.copy('node_modules/@yaireo/tagify/dist/tagify.polyfills.min.js', 'pagemarks/js/vendor/')
    shutil.copy('node_modules/crypto-es/lib/sha1.js', 'pagemarks/js/vendor/')
    shutil.copy('node_modules/crypto-es/lib/core.js', 'pagemarks/js/vendor/')
    print('OK', flush=True)



def main() -> int:
    rc = ensure_root()
    if rc == 0:
        rc = run_npm_ci()
    if rc == 0:
        rc = compile_sass()
    if rc == 0:
        copy_tagify()
    return rc



if __name__ == '__main__':
    sys.exit(main())
